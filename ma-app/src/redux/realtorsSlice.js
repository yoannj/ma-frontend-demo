const initialState = {
  agency: '',
  agencies: [],
  messages: [],
};

const RealtorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'agencies/set': {
      return {
        ...state,
        agencies: [...action.agencies],
      };
    }
    case 'agency/update': {
      const agencies = [...state.agencies];
      const agencyIndex = state.agencies.findIndex((agency) => agency.id === action.agency.id);

      agencies[agencyIndex] = action.agency;
      return {
        ...state,
        agencies,
      };
    }
    case 'agencies/select': {
      return {
        ...state,
        agency: Number(action.agency),
      };
    }
    case 'messages/set': {
      return {
        ...state,
        messages: [...action.messages],
      };
    }
    case 'messages/loadMore': {
      return {
        ...state,
        messages: [...state.messages, ...action.messages],
      };
    }
    case 'messages/update': {
      const messages = [...state.messages];
      const messageIndex = state.messages.findIndex((message) => message.id === action.message.id);

      messages[messageIndex] = {
        ...messages[messageIndex],
        ...action.message,
      };

      return {
        ...state,
        messages: [...messages],
      };
    }
    default:
      return state;
  }
};

export default RealtorsReducer;
