// eslint-disable-next-line import/no-extraneous-dependencies
import { combineReducers } from 'redux';
import RealtorsReducer from './realtorsSlice';

const rootReducer = combineReducers({
  realtors: RealtorsReducer,
});

export default rootReducer;
