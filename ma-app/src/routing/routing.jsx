import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ROUTES from './routes';
import Inbox from '../components/inbox/inbox';

const Routing = () => {
  return (
    <Switch>
      <Route exact path={ROUTES.home} />
      <Route path={ROUTES.agency.format(':agenceId')} render={() => <Inbox />} />
      <Route exact path={ROUTES.error_404} render={() => <>404</>} />
    </Switch>
  );
};

export default Routing;
