/* eslint no-extend-native: ['error', { 'exceptions': ['String'] }] */
String.prototype.format = function format() {
  let i = 0;
  // eslint-disable-next-line prefer-rest-params
  const args = arguments;
  return this.replace(/{}/g, () => {
    return args[i] !== 'undefined' ? args[i++] : '';
  });
};

const ROUTES = {
  home: '/',
  agency: '/agence/{}',
  agency_message_detail: '/agence/{}/message/{}',
  agency_message_detail_404: '/agence/{}/404',
  error_404: '/404',
};

export default ROUTES;
