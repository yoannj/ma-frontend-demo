const textTruncate = (str) => {
  const length = 70;
  if (str.length > length) {
    return `${str.substring(0, length)} ...`;
  }
  return str;
};

// eslint-disable-next-line import/prefer-default-export
export { textTruncate };
