import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import MaLogo from '../../assets/logo-meilleursagents.svg';
import './header.scss';

const Header = ({ agencies, selectedAgency, selectAgency }) => {
  const [agencyDetails, setAgencyDetails] = useState();
  useEffect(() => {
    setAgencyDetails(
      agencies.find((agency) => {
        return agency.id === selectedAgency;
      })
    );
  }, [selectedAgency, agencies]);

  return (
    <header className="Header" data-testid="header">
      <img src={MaLogo} alt="Logo" className="Logo" />

      <div className="Header-actions">
        {selectedAgency ? (
          <div className="Header-unread">
            <i className="mypro-icon mypro-icon-inbox" />{' '}
            {agencyDetails ? agencyDetails.unread_messages : null}
          </div>
        ) : null}

        <label htmlFor="agenceInput">Agence :</label>
        <select
          id="agenceInput"
          data-testid="select-agency"
          value={selectedAgency}
          aria-label="Selectionnez une agence"
          aria-required="true"
          onChange={(e) => selectAgency(e.target.value)}
        >
          <option value="">Agence</option>
          {agencies.map((agency, i) => {
            return (
              <option value={agency.id} key={i} data-testid="select-agency-option">
                {agency.name}
              </option>
            );
          })}
        </select>
      </div>
    </header>
  );
};

Header.propTypes = {
  agencies: PropTypes.array.isRequired,
  selectedAgency: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  selectAgency: PropTypes.func.isRequired,
};

export default Header;
