import React from 'react';
import { render, fireEvent } from '../../test-utils';
import Header from './header';

test('selectOptions', () => {
  const agencies = [
    {
      id: 101,
      logo: 'http://placehold.it/100x100?text=Agence+101',
      name: 'Agence #101',
      unread_messages: 60,
    },
    {
      id: 102,
      logo: 'http://placehold.it/100x100?text=Agence+102',
      name: 'Agence #102',
      unread_messages: 60,
    },
    {
      id: 103,
      logo: 'http://placehold.it/100x100?text=Agence+103',
      name: 'Agence #103',
      unread_messages: 82,
    },
  ];
  const selectAgency = (data) => data;
  const { getByTestId, getAllByTestId } = render(
    <Header agencies={agencies} selectedAgency="101" selectAgency={selectAgency}/>
  );
  // The value should be the key of the option
  fireEvent.change(getByTestId('select-agency'), { target: { value: 101 } });
  const options = getAllByTestId('select-agency-option');
  expect(options[0].selected).toBeTruthy();
  expect(options[1].selected).toBeFalsy();
  expect(options[2].selected).toBeFalsy();
});
