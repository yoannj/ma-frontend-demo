import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory, Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import ROUTES from '../../routing/routes';
import { getAllMessages } from '../../api/realtors';
import MessageDetails from '../messageDetails/messageDetails';
import MessagePreview from '../messagePreview/messagePreview';
import useInfiniteScroll from '../../utils/useInfiniteScroll';
import inboxFormattedData from './inboxPresenter';
import './inbox.scss';

const Inbox = () => {
  const { agenceId } = useParams();
  const messages = useSelector((state) => state.realtors.messages);
  const refsMessages = useRef([React.createRef(), React.createRef()]);

  const dispatch = useDispatch();
  const [isFetching, setIsFetching] = useState(false);
  const [page, setPage] = useState(1);
  const [HasMore, setHasMore] = useState(true);
  const history = useHistory();

  const loadMessages = (first = false) => {
    setIsFetching(true);
    getAllMessages(agenceId, page)
      .then((results) => {
        setPage((prevPageNumber) => prevPageNumber + 1);
        setHasMore(results.data.length > 0);
        setIsFetching(false);
        const formattedData = inboxFormattedData(results.data);

        dispatch({
          type: first ? 'messages/set' : 'messages/loadMore',
          messages: formattedData,
        });
      })
      .catch((error) => {
        if (error.response.status === 404) {
          history.push(ROUTES.error_404);
        }
      });
  };

  const [lastElementRef] = useInfiniteScroll(HasMore ? loadMessages : () => {}, isFetching);

  useEffect(() => {
    setIsFetching(true);
    dispatch({
      type: 'agencies/select',
      agency: agenceId,
    });

    loadMessages(true);
  }, []);

  return (
    <>
      <Helmet>
        <title>Boite de réception | Meilleurs agent</title>
      </Helmet>
      <dl className="Inbox-messages">
        {messages.map((message, index) => {
          if (message.length === index + 1) {
            return (
              <div
                ref={refsMessages.current([index])}
                className="Inbox-messages-item"
                key={message.id}
              >
                <MessagePreview message={message} />
              </div>
            );
          }
          return (
            <div ref={lastElementRef} className="Inbox-messages-item" key={message.id}>
              <MessagePreview message={message} />
            </div>
          );
        })}
        {isFetching && <p>Fetching items...</p>}
      </dl>
      <div className="Inbox-details">
        <Switch>
          <Route
            exact
            path={ROUTES.agency_message_detail.format(':agenceId', ':messageId')}
            render={() => <MessageDetails />}
          />
          <Route
            exact
            path={ROUTES.agency_message_detail_404.format(':agenceId')}
            render={() => <>404</>}
          />
        </Switch>
      </div>
    </>
  );
};

export default Inbox;
