import inboxFormattedData from './inboxPresenter';

test('Inbox presenter', () => {
  const messages = [
    {
      // eslint-disable-next-line max-len
      body: "Lorem Ipsum #10145 is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      contact: {
        email: 'tanderson@gmail.com',
        firstname: 'Thomas',
        lastname: 'Anderson',
        phone: '0694180396',
      },
      date: '2021-07-07T19:35:41.818641',
      id: 10145,
      read: true,
      subject: 'SMS #10145',
      type: 'email',
    },
    {
      // eslint-disable-next-line max-len
      body: "Lorem Ipsum #10145 is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      contact: {
        email: 'tanderson@gmail.com',
        firstname: 'Thomas',
        lastname: 'Anderson',
        phone: '0694180396',
      },
      date: '2021-07-07T19:35:41.818641',
      id: 10145,
      read: true,
      subject: 'SMS #10145',
      type: 'phone',
    },
    {
      // eslint-disable-next-line max-len
      body: "Lorem Ipsum #10149 is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      contact: {
        email: 'tsmith@gmail.com',
        firstname: 'Thomas',
        lastname: 'Smith',
        phone: '0632225265',
      },
      date: '2021-07-07T19:35:41.818765',
      id: 10149,
      read: false,
      subject: 'SMS #10149',
      type: 'sms',
    },
  ];

  const result = [
    {
      // eslint-disable-next-line max-len
      body: "Lorem Ipsum #10145 is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      contact: {
        email: 'tanderson@gmail.com',
        firstname: 'Thomas',
        lastname: 'Anderson',
        phone: '0694180396',
      },
      date: '2021-07-07T19:35:41.818641',
      id: 10145,
      read: true,
      subject: 'SMS #10145',
      type: 'email',
      _icon: 'mypro-icon-mail',
      _messageType: 'Message',
    },
    {
      // eslint-disable-next-line max-len
      body: "Lorem Ipsum #10145 is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      contact: {
        email: 'tanderson@gmail.com',
        firstname: 'Thomas',
        lastname: 'Anderson',
        phone: '0694180396',
      },
      date: '2021-07-07T19:35:41.818641',
      id: 10145,
      read: true,
      subject: 'SMS #10145',
      type: 'phone',
      _icon: 'mypro-icon-phone',
      _messageType: 'Message vocale',
    },
    {
      // eslint-disable-next-line max-len
      body: "Lorem Ipsum #10149 is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      contact: {
        email: 'tsmith@gmail.com',
        firstname: 'Thomas',
        lastname: 'Smith',
        phone: '0632225265',
      },
      date: '2021-07-07T19:35:41.818765',
      id: 10149,
      read: false,
      subject: 'SMS #10149',
      type: 'sms',
      _icon: 'mypro-icon-sms',
      _messageType: 'SMS',
    },
  ];

  expect(inboxFormattedData(messages)).toStrictEqual(result);
});
