const inboxFormattedData = (messages) => {
  return messages.map((message) => {
    let icon = 'mypro-icon-mail';
    let messageType = 'Message';

    switch (message.type) {
      case 'sms':
        icon = 'mypro-icon-sms';
        messageType = 'SMS';
        break;
      case 'phone':
        icon = 'mypro-icon-phone';
        messageType = 'Message vocale';
        break;
      default:
        break;
    }

    return {
      ...message,
      _icon: icon,
      _messageType: messageType,
    };
  });
};

export default inboxFormattedData;
