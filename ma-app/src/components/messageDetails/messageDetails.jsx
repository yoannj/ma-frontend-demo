import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';
import { Helmet } from 'react-helmet';
import { getAgency, patchMessage } from '../../api/realtors';
import './messageDetails.scss';
import ROUTES from '../../routing/routes';

const MessageDetails = () => {
  const { agenceId, messageId } = useParams();
  const [message, setMessage] = useState();
  const dispatch = useDispatch();
  const history = useHistory();
  let icon = 'mypro-icon-mail';

  if (message && message.type === 'sms') {
    icon = 'mypro-icon-sms';
  }

  if (message && message.type === 'phone') {
    icon = 'mypro-icon-phone';
  }

  useEffect(() => {
    patchMessage(agenceId, messageId, true)
      .then((results) => {
        dispatch({
          type: 'messages/update',
          message: results.data,
        });
        getAgency(agenceId).then((agency) => {
          dispatch({
            type: 'agency/update',
            agency: agency.data,
          });
        });
        setMessage(results.data);
      })
      .catch((error) => {
        if (error.response.status === 404) {
          history.push(ROUTES.agency_message_detail_404.format(agenceId));
        }
      });
  }, [messageId]);

  return message ? (
    <>
      <Helmet>
        <title>{message.subject} | Meilleurs agent</title>
      </Helmet>
      <div className="MessageDetails">
        <div className="MessageDetails-header">
          <div className="MessageDetails-header-type">
            <i className={`mypro-icon ${icon}`} />
          </div>
          <div className="MessageDetails-header-name">
            {message.contact.firstname} {message.contact.lastname}
          </div>
          <div className="MessageDetails-header-mailLabel">Email</div>
          <div className="MessageDetails-header-mailvalue">
            <a href={`mailto:${message.contact.email}`}>{message.contact.email}</a>
          </div>
          <div className="MessageDetails-header-phoneLabel">Téléphone</div>
          <div className="MessageDetails-header-phoneValue">{message.contact.phone}</div>
        </div>

        <div className="MessageDetails-content">
          <div className="MessageDetails-content-name">
            {message.contact.firstname} {message.contact.lastname}
          </div>
          <div className="MessageDetails-content-date">
            {format(new Date(message.date), 'd LLLL', { locale: fr })} à{' '}
            {format(new Date(message.date), 'H:m', { locale: fr })}
          </div>
          <div className="MessageDetails-content-message">{message.body}</div>
        </div>
      </div>
    </>
  ) : (
    <>Loading</>
  );
};

export default MessageDetails;
