import React from 'react';
import PropTypes from 'prop-types';
import { formatRelative, subDays } from 'date-fns';
import { fr } from 'date-fns/locale';
import { Link, useParams } from 'react-router-dom';
import { textTruncate } from '../../utils/formatter';
import ROUTES from '../../routing/routes';
import './messagePreview.scss';

const MessagePreview = ({ message }) => {
  const { agenceId } = useParams();

  return (
    <Link
      className={`MessagePreview ${message.read ? 'MessagePreview--read' : ''}`}
      to={ROUTES.agency_message_detail.format(agenceId, message.id)}
      tabIndex="-1"
    >
      <div className={`MessagePreview-type ${message.read ? 'MessagePreview-type--read' : ''}`}>
        <i className={`mypro-icon ${message._icon}`} />
      </div>
      <div className="MessagePreview-content">
        <div className="MessagePreview-title">
          <dt>
            {message.contact.firstname} {message.contact.lastname}{' '}
            <small>({message.contact.phone})</small>
          </dt>

          <div className={`MessagePreview-date ${message.read ? 'MessagePreview-date--read' : ''}`}>
            {formatRelative(subDays(new Date(message.date), 3), new Date(), { locale: fr })}
          </div>
        </div>
        <div>{message._messageType} sur votre vitrine Meilleurs Agents</div>
        <dd className="MessagePreview-summary">{textTruncate(message.body)}</dd>
      </div>
    </Link>
  );
};

MessagePreview.propTypes = {
  message: PropTypes.object.isRequired,
};

export default MessagePreview;
