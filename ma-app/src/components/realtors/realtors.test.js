// realtors.test.js
// import { render, screen } from '@testing-library/react';
import React from 'react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { render, screen } from '../../test-utils';

import '@testing-library/jest-dom/extend-expect';

import Realtors from './realtors';

test('App rendering', () => {
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <Realtors />
    </Router>
  );

  expect(screen.getByTestId('realtors')).toBeInTheDocument();
});
