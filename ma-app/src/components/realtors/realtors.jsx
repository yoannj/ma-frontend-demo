import React, { useEffect } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet';
import { getAllAgencies, getAllMessages } from '../../api/realtors';
import Header from '../header/header';
import Routing from '../../routing/routing';
import ROUTES from '../../routing/routes';

const Realtors = () => {
  const agencies = useSelector((state) => state.realtors.agencies);
  const agency = useSelector((state) => state.realtors.agency);
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch({
    path: ROUTES.agency_message_detail.format(':agenciId', ':messageId'),
    strict: true,
    sensitive: true,
  });

  useEffect(() => {
    getAllAgencies().then((results) =>
      dispatch({
        type: 'agencies/set',
        agencies: results.data,
      })
    );
  }, []);

  const selectAgency = (agencyId) => {
    getAllMessages(agencyId).then((results) =>
      dispatch({
        type: 'messages/set',
        messages: results.data,
      })
    );

    dispatch({
      type: 'agencies/select',
      agency: agencyId,
    });
    history.push(`/agence/${agencyId}`);
  };
  return (
    <>
      <Helmet>
        <title>Meilleurs agent</title>
      </Helmet>
      <div className={`Ma-container ${match ? 'message-open' : ''}`} data-testid="realtors">
        <Header agencies={agencies} selectedAgency={agency} selectAgency={selectAgency} />
        <Routing />
      </div>
    </>
  );
};

export default Realtors;
