import axios from 'axios';

/* eslint no-extend-native: ['error', { 'exceptions': ['String'] }] */
String.prototype.format = function format() {
  let i = 0;
  // eslint-disable-next-line prefer-rest-params
  const args = arguments;
  return this.replace(/{}/g, () => {
    return args[i] !== 'undefined' ? args[i++] : '';
  });
};

const REALTORS_HOST = 'http://localhost:8080';

const API_REALTORS_MAP = {
  all_realtors: `${REALTORS_HOST}/realtors/`,
  realtor: `${REALTORS_HOST}/realtors/{}`,
  all_messages: `${REALTORS_HOST}/realtors/{}/messages/`,
  message: `${REALTORS_HOST}/realtors/{}/messages/{}`,
};

const getAllAgencies = () => {
  return axios.get(API_REALTORS_MAP.all_realtors);
};

const getAgency = (agencyId) => {
  return axios.get(API_REALTORS_MAP.realtor.format(agencyId));
};

const getAllMessages = (agencyId, page) => {
  const params = {
    params: {
      sort: 'date:desc',
      page,
      page_size: 10,
    },
  };
  return axios.get(API_REALTORS_MAP.all_messages.format(agencyId), params);
};

const getMessage = (agencyId, messageId) => {
  return axios.get(API_REALTORS_MAP.message.format(agencyId, messageId));
};

const patchMessage = (agencyId, messageId, read) => {
  const params = {
    read,
  };
  return axios.patch(API_REALTORS_MAP.message.format(agencyId, messageId), params);
};

export { getAllAgencies, getAgency, getMessage, getAllMessages, patchMessage };
